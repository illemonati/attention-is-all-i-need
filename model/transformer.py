import torch
import torch.nn as nn

from model.decoder import Decoder
from model.encoder import Encoder
from model.module import DeviceModule


class Transformer(nn.Module):
    def __init__(self, input_nclasses, target_nclasses, input_pad_index, target_pad_index, embedding_size=512,
                 nlayers=6, nheads=8, forward_expansion=4, dropout=0.1, max_len=100):
        super(Transformer, self).__init__()

        self.encoder = Encoder(input_nclasses, embedding_size, nlayers, nheads, forward_expansion, dropout, max_len)
        self.decoder = Decoder(target_nclasses, embedding_size, nlayers, nheads, forward_expansion, dropout, max_len)

        self.input_pad_index = input_pad_index
        self.target_pad_index = target_pad_index

    def get_device(self):
        return next(self.parameters()).device

    def create_input_mask(self, input):
        num_samples, input_len = input.shape[0], input.shape[1]
        # (num_samples, 1, 1, input_len)
        input_mask = (input != self.input_pad_index).reshape((num_samples, 1, 1, input_len)).to(self.get_device())
        return input_mask

    def create_target_mask(self, target):
        num_samples, target_len = target.shape[0], target.shape[1]
        target_mask = torch.tril(torch.ones((target_len, target_len))).expand(num_samples, 1, target_len, target_len).to(self.get_device())
        return target_mask

    def forward(self, input, target):
        input_mask = self.create_input_mask(input)
        target_mask = self.create_target_mask(target)
        encoded_input = self.encoder(input, input_mask)
        decoded_target = self.decoder(target, encoded_input, target_mask, input_mask)
        return decoded_target
