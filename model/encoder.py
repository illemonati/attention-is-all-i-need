import torch
import torch.nn as nn

from model.module import DeviceModule
from model.transformer_block import TransformerBlock


class Encoder(nn.Module):
    def __init__(self, input_nclasses, embedding_size, nlayers, nheads, forward_expansion, dropout, max_len):
        super(Encoder, self).__init__()
        self.embedding_size = embedding_size
        self.content_embedding = nn.Embedding(input_nclasses, embedding_size)
        self.position_embedding = nn.Embedding(max_len, embedding_size)
        self.layers = nn.ModuleList(
            [TransformerBlock(embedding_size, nheads, dropout, forward_expansion) for _ in range(nlayers)])
        self.dropout = nn.Dropout(dropout)

    def get_device(self):
        return next(self.parameters()).device

    def forward(self, input, mask=None):
        num_samples, seq_len = input.shape[0], input.shape[1]
        positions = torch.arange(0, seq_len).expand(num_samples, seq_len).to(self.get_device())
        combined_embedding = self.content_embedding(input) + self.position_embedding(positions)
        out = self.dropout(combined_embedding)
        for layer in self.layers:
            out = layer(out, out, out, mask)
        return out
