import torch
import torch.nn as nn
import torch.nn.functional as F

from model.attention import MultiHeadSelfAttention
from model.module import DeviceModule


class TransformerBlock(nn.Module):
    def __init__(self, embedding_size, nheads, dropout, forward_expansion):
        super(TransformerBlock, self).__init__()
        self.attention = MultiHeadSelfAttention(embedding_size, nheads)
        self.norm0 = nn.LayerNorm(embedding_size)
        self.norm1 = nn.LayerNorm(embedding_size)

        self.feed_forward = nn.Sequential(
            nn.Linear(embedding_size, forward_expansion*embedding_size),
            nn.ReLU(),
            nn.Linear(forward_expansion*embedding_size, embedding_size)
        )

        self.dropout = nn.Dropout(dropout)

    def forward(self, Q, K, V, mask=None):
        # (num_samples, Q_len, embedding_size)
        attention = self.attention(Q, K, V, mask)

        # both are (num_samples, Q_len, embedding_size) so we can just add
        attention_with_skip_connection = attention + Q
        processed_attention_with_skip_connection = self.dropout(self.norm0(attention_with_skip_connection))

        forward_output = self.feed_forward(processed_attention_with_skip_connection)
        forward_output_with_skip_connection = forward_output + processed_attention_with_skip_connection
        out = self.dropout(self.norm1(forward_output_with_skip_connection))

        return out




