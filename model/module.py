import torch.nn as nn

class DeviceModule(nn.Module):
    def __init__(self):
        super(DeviceModule, self).__init__()

    def get_device(self):
        return next(self.parameters()).device
