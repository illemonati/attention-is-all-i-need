import torch
import torch.nn as nn

from model.decoder_block import DecoderBlock
from model.module import DeviceModule


class Decoder(nn.Module):
    def __init__(self, target_nclasses, embedding_size, nlayers, nheads, forward_expansion, dropout, max_len):
        super(Decoder, self).__init__()
        self.content_embedding = nn.Embedding(target_nclasses, embedding_size)
        self.position_embedding = nn.Embedding(max_len, embedding_size)

        self.layers = nn.ModuleList(
            [DecoderBlock(embedding_size, nheads, dropout, forward_expansion) for _ in range(nlayers)])

        self.out = nn.Linear(embedding_size, target_nclasses)
        self.dropout = nn.Dropout(dropout)
        # self.relu = nn.ReLU()
        # self.softmax = nn.Softmax(dim=-1)

    def get_device(self):
        return next(self.parameters()).device

    def forward(self, input, encoder_output, target_mask, input_mask=None):
        num_samples, seq_len = input.shape[0], input.shape[1]
        positions = torch.arange(0, seq_len).expand(num_samples, seq_len).to(self.get_device())
        combined_embedding = self.content_embedding(input) + self.position_embedding(positions)
        out = self.dropout(combined_embedding)
        for layer in self.layers:
            out = layer(out, encoder_output, encoder_output, target_mask, input_mask)
        out = self.out(out)
        # out = self.softmax(out)
        return out
