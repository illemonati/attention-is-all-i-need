import torch
import torch.nn as nn
import torch.nn.functional as F

from model.module import DeviceModule
from model.attention import MultiHeadSelfAttention
from model.transformer_block import TransformerBlock


class DecoderBlock(nn.Module):
    def __init__(self, embedding_size, nheads, dropout, forward_expansion):
        super(DecoderBlock, self).__init__()
        self.attention = MultiHeadSelfAttention(embedding_size, nheads)
        self.norm = nn.LayerNorm(embedding_size)
        self.transformer_block = TransformerBlock(embedding_size, nheads, dropout, forward_expansion)
        self.dropout = nn.Dropout(dropout)

    def forward(self, input, K, V, target_mask, input_mask=None):
        attention = self.attention(input, input, input, target_mask)
        attention_with_skip_connection = attention + input
        Q = self.dropout(self.norm(attention_with_skip_connection))
        out = self.transformer_block(Q, K, V, input_mask)
        return out
