import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from model.module import DeviceModule


class MultiHeadSelfAttention(nn.Module):
    def __init__(self, embedding_size, nheads):
        super(MultiHeadSelfAttention, self).__init__()
        self.embedding_size = embedding_size
        self.nheads = nheads

        # must have same size for embed per head
        assert embedding_size % nheads == 0

        self.embedding_size_per_head = self.embedding_size // self.nheads

        self.K_linear = nn.Linear(
            self.embedding_size, self.embedding_size, False)
        self.Q_linear = nn.Linear(
            self.embedding_size, self.embedding_size, False)
        self.V_linear = nn.Linear(
            self.embedding_size, self.embedding_size, False)

        self.out = nn.Linear(self.embedding_size, self.embedding_size, True)

    def forward(self, Q, K, V, mask=None):
        num_samples = Q.shape[0]
        Q_len, K_len, V_len = Q.shape[1], K.shape[1], V.shape[1]

        head_Qs = self.Q_linear(Q)
        head_Ks = self.K_linear(K)
        head_Vs = self.V_linear(V)

        # split the embedding into heads
        # basically goes from (num_samples, _len, embedding_size)
        # to (num_samples, _len, nheads, embedding_size_per_head)
        head_Qs = head_Qs.reshape(
            (num_samples, Q_len, self.nheads, self.embedding_size_per_head))
        head_Ks = head_Ks.reshape(
            (num_samples, K_len, self.nheads, self.embedding_size_per_head))
        head_Vs = head_Vs.reshape(
            (num_samples, V_len, self.nheads, self.embedding_size_per_head))



        # the video called this energy
        # this term is not mentioned in the paper, but is QK^T
        # energy shape is (num_samples, nheads, Q_len, K_len)
        # N: num_samples
        # Q: Q_len
        # K: K_len
        # H: nheads
        # S: embedding_size_per_head
        energy = torch.einsum("NQHS, NKHS -> NHQK", [head_Qs, head_Ks])

        if mask is not None:
            energy = energy.masked_fill(mask == 0, -1e9)

        # this is the QK^R / sqrt(d_k) from the paper
        scaled_energy = energy / self.embedding_size**(1/2)

        # 3rd dim is the K_len, the K of NHQK
        softmaxed_energy = F.softmax(scaled_energy, dim=3)

        # N: num_samples
        # Q: Q_len
        # K: K_len
        # V: V_len
        # H: nheads
        # S: embedding_size_per_head
        # (num_samples, Q_len, nheads, embedding_size_per_head)
        scaled_dot_product_attention = torch.einsum("NHQK, NVHS -> NQHS", [softmaxed_energy, head_Vs])

        # flatten the attention from all heads
        # (num_samples, Q_len, embedding_size)
        flattened_attention = scaled_dot_product_attention.reshape((num_samples, Q_len, self.embedding_size))

        output = self.out(flattened_attention)
        return output
