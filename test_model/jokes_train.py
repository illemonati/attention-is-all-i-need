import csv
import pickle
import re
import numpy as np
import torch
import os
import sys
import torch.nn as nn
import torch.nn.functional as F
from data_loader import DataLoader
from torchviz import make_dot

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(os.path.join(parent_dir))

from model.transformer import Transformer


EPS = 30
BATCH_SIZE = 1024

if __name__ == "__main__":
    data = DataLoader()
    data.load_data()

    # device = "cuda" if torch.cuda.is_available() else "mps" if torch.backends.mps.is_available() else "cpu"
    device = "cpu"
    # print(device)

    X = torch.from_numpy(data.sequences.astype("long")).to(device)
    y = torch.from_numpy(data.labels.astype("long")).to(device)
    # one_hot_y = torch.from_numpy(np.expand_dims(data.one_hot_labels, axis=1).astype("long")).to(device)
    one_hot_y = torch.from_numpy(data.one_hot_labels.astype("long")).to(device)

    # print(X.shape)
    # print(y.shape)

    seq_pad_index = 1
    label_pad_index = 1


    model = Transformer(data.words, data.words, seq_pad_index, label_pad_index, embedding_size=512, nheads=2, nlayers=2, forward_expansion=2, max_len=X.shape[-1]).to(device)
    # model(X, y)

    print(model)
    # print(dict(list(model.named_parameters())))

    # make_dot(one_hot_y, params=dict(list(model.named_parameters()))).render("transformer", format="png")

    optimizer = torch.optim.Adam(model.parameters())
    criterion = nn.CrossEntropyLoss()
    batches = int(np.floor(len(data.sequences) / BATCH_SIZE))
    for ep in range(EPS):
        ep_loss = 0.0
        for batch_num in range(batches):
            batchX = X[batch_num*BATCH_SIZE:BATCH_SIZE*(batch_num+1)]
            batchY = y[batch_num*BATCH_SIZE:BATCH_SIZE*(batch_num+1)]
            batchOneHotY = one_hot_y[batch_num*BATCH_SIZE:BATCH_SIZE*(batch_num+1)]

            optimizer.zero_grad()
            # print(batchY[:, :-1])
            outputs = model.forward(batchX, batchY[:, :-1])
            # print(outputs.shape, outputs.dtype)
            # print(batchY.shape, batchY.dtype)
            ofloat = outputs.float()
            # print(batchY.shape)
            # yfloat = batchOneHotY
            yfloat = batchOneHotY[:, 1:].float()
            # print("o", ofloat.shape)
            # print("y", yfloat.shape)
            # print(ofloat)
            # print(yfloat)

            loss = criterion(ofloat, yfloat)
            # loss.requires_grad = True
            number_loss = loss.item()
            loss.backward()
            optimizer.step()
            ep_loss += number_loss / batches
            if batch_num % 5 == 0:
                print(f"Batch [{batch_num}] loss: {number_loss}")

        print(f'[{ep}] loss: {ep_loss}')
        torch.save(model, os.path.join(parent_dir, "model_saves", f"jokes-ep-{ep}.pt"))
