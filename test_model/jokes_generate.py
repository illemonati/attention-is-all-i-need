import torch

from data_loader import DataLoader
import numpy as np


starting_phrase = ["why", "did", "the", "cow", "cross"]
MAX_LEN = 30
MODEL_FILE = "./model_saves/jokes-ep-11.pt"
SEQ_LEN = 4

if __name__ == '__main__':
    data_loader = DataLoader()
    data_loader.load_data(cache=True)

    model = torch.load(MODEL_FILE)

    encoded_start = torch.from_numpy(np.array([data_loader.encode_joke(starting_phrase)]))
    print(encoded_start)

    input = encoded_start
    print(input[:, :-1].shape)
    dummy_o = torch.from_numpy(data_loader.labels[0:1])

    full_generation = [*starting_phrase]

    for _ in range(MAX_LEN):
        output = model(input, input[:, 1:])[0]
        output_words = [int(torch.argmax(o).detach()) for o in output]
        latest_word = output_words[-1]
        output_words_in_words = [data_loader.reverse_word_dic[o] for o in output_words]
        latest_word_in_words = output_words_in_words[-1]
        full_generation.append(latest_word_in_words)
        if latest_word_in_words == "[END]":
            break
        print(output_words_in_words)
        input = torch.from_numpy(np.array([[*input[0][1:], latest_word]]))
        print(input)

    print(full_generation)
    print(' '.join(full_generation))
