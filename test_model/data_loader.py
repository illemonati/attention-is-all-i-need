import csv
import pickle
import re
import numpy as np
import torch
import os
import sys


current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(os.path.join(parent_dir))


class DataLoader:
    raw_jokes = []
    processed_jokes = []
    encoded_jokes = []
    word_dic = {"[END]": 0, "[PADDING]": 1}
    reverse_word_dic = {}
    words = 0
    sequences = []
    labels = []
    one_hot_labels = []
    seq_len = 6

    punctuation = "'!\"#$%&'()*+,-./:;?@[\]^_`{|}~’ "
    punctuation_pattern = '(' + '|'.join(map(re.escape, punctuation)) + ')'

    def __init__(self):
        self.reverse_word_dic = {v: k for k, v in self.word_dic.items()}
        self.words = len(self.word_dic)

    def process_joke(self, joke):
        joke = joke.lower()
        joke = [q for q in (a.strip() for a in re.split(self.punctuation_pattern, joke)) if q != '']
        joke = [*joke, "[END]"]
        self.processed_jokes.append(joke)
        self.encoded_jokes.append(self.encode_joke(joke))

    def encode_joke(self, joke):
        encoded_joke = []
        for word in joke:
            if word in self.word_dic:
                word_num = self.word_dic[word]
            else:
                word_num = self.words
                self.words += 1
                self.word_dic[word] = word_num
                self.reverse_word_dic[word_num] = word
            encoded_joke.append(word_num)
        return encoded_joke

    def to_one_hot(self, label):
        one_hot = np.zeros(self.words)
        one_hot[label] = 1
        return one_hot

    def create_training(self, joke):
        for i in range(len(joke) - self.seq_len):
            seq = joke[i:i + self.seq_len]
            # label = joke[i+1:i + self.seq_len+1]
            label = joke[i+1:i + self.seq_len+1]
            one_hot_label = [self.to_one_hot(x) for x in label]

            self.sequences.append(seq)
            self.labels.append(label)
            self.one_hot_labels.append(one_hot_label)

    def use_cache(self, mode: "save" or "load" = "save") -> bool:
        try:
            with open(os.path.join(parent_dir, "data", "jokes-cache.pickle"), "wb") as file:
                if mode == "save":
                    pickle.dump(self.__dict__, file)
                elif mode == "load":
                    self.__dict__.update(pickle.load(file))
            return True
        except Exception as e:
            return False

    def load_data(self, cache: bool = False):
        if not cache or not self.use_cache("load"):
            with open("./data/jokes.csv") as file:
                reader = csv.reader(file, delimiter=',', quotechar='"')
                for row in reader:
                    joke = row[1]
                    self.raw_jokes.append(joke)
                    self.process_joke(joke)
            for joke in self.encoded_jokes:
                self.create_training(joke)
            self.sequences = np.array(self.sequences)
            self.labels = np.array(self.labels)
            self.one_hot_labels = np.array(self.one_hot_labels)
            self.use_cache("save")